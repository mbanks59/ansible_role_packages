import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('centos')


def test_installed_packages(host):
    packages = host.pip_package.get_packages()
    assert packages['pip-install-test']['version'] == "0.5"
